﻿Imports System.Runtime.InteropServices
Imports System
Imports System.Security.Cryptography
Imports System.Text
Imports System.IO
<ComVisible(True), ClassInterface(ClassInterfaceType.None),
GuidAttribute("9149A48D-AB15-4B96-AF50-9DF60494BC3B")> _
Public Class ExchangeUfl : Implements IExchangeUfl
    Private Shared memStream As MemoryStream = New MemoryStream
    'key for necta
    Private b64key As String = "06W9tLEMdwxKv1C6XMoel7ibhix9dPpwJqdZ+7EuMMU="

    Private b64iv As String = "dIOh4fhx5iL6JzGw6b2B1yp56jxShQqwo2U2G3GqI3s="

    Private textConverter As ASCIIEncoding = New ASCIIEncoding

    Private myRijndael As RijndaelManaged = New RijndaelManaged

    Public Sub New()
        MyBase.New()
    End Sub
    'Public Function ConvertUSDollarsToCDN1(ByVal usd As Double) As Double Implements IExchangeUfl.ConvertUSDollarsToCDN
    '    If usd > Double.MaxValue Then
    '        Throw New Exception("Value submitted is larger than the maximum value allowed for a double.")
    '    End If
    '    Return (usd * 1.45)
    'End Function

    'Public Function CrystalDecrypt(ByVal decData As String) As String Implements IExchangeUfl.CrystalDecrypt
    '    Try
    '        Dim rijKey() As Byte = Convert.FromBase64String(Me.b64key)
    '        Dim rijIv() As Byte = Convert.FromBase64String(Me.b64iv)
    '        Dim encrypted() As Byte
    '        encrypted = Convert.FromBase64String(decData)
    '        Me.myRijndael.BlockSize = 256
    '        Me.myRijndael.KeySize = 256
    '        Dim decryptor As ICryptoTransform = Me.myRijndael.CreateDecryptor(rijKey, rijIv)
    '        Dim msDecrypt As MemoryStream = New MemoryStream(encrypted)
    '        Dim csDecrypt As CryptoStream = New CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)
    '        Dim fromEncrypt() As Byte
    '        fromEncrypt = New Byte((encrypted.Length) - 1) {}
    '        csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length)
    '        Dim roundtrip As String = Me.textConverter.GetString(fromEncrypt)
    '        Return roundtrip
    '    Catch ex As Exception
    '        Return ex.Message
    '    End Try
    'End Function

    Public Function CrystalEncrypt(ByVal encData As String) As String Implements IExchangeUfl.CrystalEncrypt
        Try
            Dim rijKey() As Byte = Convert.FromBase64String(b64key)
            Dim rijIv() As Byte = Convert.FromBase64String(b64iv)
            myRijndael.BlockSize = 256
            myRijndael.KeySize = 256
            Dim encryptor As ICryptoTransform = myRijndael.CreateEncryptor(rijKey, rijIv)
            Dim msEncrypt As MemoryStream = New MemoryStream
            Dim csEncrypt As CryptoStream = New CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)
            Dim toEncrypt() As Byte
            toEncrypt = textConverter.GetBytes(encData)
            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length)
            csEncrypt.FlushFinalBlock()
            Dim encrypted() As Byte
            encrypted = msEncrypt.ToArray
            Dim cipherText As String = Convert.ToBase64String(encrypted, 0, encrypted.Length)
            Return cipherText
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
